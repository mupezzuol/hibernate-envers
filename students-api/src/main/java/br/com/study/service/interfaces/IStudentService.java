package br.com.study.service.interfaces;

import br.com.study.model.Student;
import br.com.study.resource.form.StudentForm;

public interface IStudentService {

	void saveStudent(Student student);

	Student updateStudent(StudentForm form);

}
