package br.com.study.service.imp;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import br.com.study.model.Student;
import br.com.study.repository.ICourseRepository;
import br.com.study.repository.IStudentRepository;
import br.com.study.resource.form.StudentForm;
import br.com.study.service.interfaces.IStudentService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class StudentService implements IStudentService {

	@Autowired
	private IStudentRepository studentRepository;

	@Autowired
	private ICourseRepository courseRepository;

	@Override
	public void saveStudent(Student student) {
		this.studentRepository.save(student);
	}

	@Override
	public Student updateStudent(StudentForm form) {
		if (form.getId() == null) {
			return this.studentRepository.save(form.converter(this.courseRepository));
		} else {
			Student studentExist = findStudent(form.getId());

			BeanUtils.copyProperties(form.converter(this.courseRepository), studentExist, "id");

			return this.studentRepository.save(studentExist);
		}

	}

	private Student findStudent(Long id) {
		Optional<Student> studentExist = this.studentRepository.findById(id);

		try {
			if (!studentExist.isPresent())
				throw new EmptyResultDataAccessException(1);
		} catch (Exception e) {
			log.error("Exception in method findStudent");
		}

		return studentExist.get();
	}

}
