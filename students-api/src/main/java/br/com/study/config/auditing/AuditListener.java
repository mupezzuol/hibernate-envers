package br.com.study.config.auditing;

import java.time.LocalDateTime;

import org.hibernate.envers.RevisionListener;

import br.com.study.model.Audity;

public class AuditListener implements RevisionListener {

	@Override
	public void newRevision(Object revisionEntity) {
		Audity revEntity = (Audity) revisionEntity;

		revEntity.setUser("Gabriel");
		revEntity.setDate(LocalDateTime.now());
	}
}
