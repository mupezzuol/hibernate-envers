package br.com.study.config.exception;

import java.util.Arrays;
import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class ApiError {

	private String field;

	private String message;

	private List<String> errors;

	public ApiError(String field, String message, String error) {
		this.field = field;
		this.message = message;
		errors = Arrays.asList(error);
	}

	public ApiError(String message) {
		this.message = message;
	}

}
