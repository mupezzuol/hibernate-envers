package br.com.study.model;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@Audited
@Entity
@Table
@AuditTable(value = "student_audit")
public class Student {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SequenceStudent")
	@SequenceGenerator(name = "SequenceStudent", sequenceName = "seq_student")
	private Long id;

	private String name;

	private Integer age;

	@Embedded
	private Address address;

	@NotAudited
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "course_id", referencedColumnName = "id")
	private Course course;

	public Student(String name, Integer age) {
		this.name = name;
		this.age = age;
//		this.address = address;
//		this.course = course;
	}

}
