package br.com.study.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Course {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SequenceCourse")
	@SequenceGenerator(name = "SequenceCourse", sequenceName = "seq_course")
	private Long id;

	@OneToOne(mappedBy = "course", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Student student;

	private String name;

	private String periodo;

	public Course(Long id) {
		this.id = id;
	}
}
