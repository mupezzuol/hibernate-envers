package br.com.study.model;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.envers.RevisionEntity;
import org.hibernate.envers.RevisionNumber;
import org.hibernate.envers.RevisionTimestamp;

import br.com.study.config.auditing.AuditListener;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity(name = "audit_info")
@RevisionEntity(AuditListener.class)
public class Audity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@RevisionNumber
	private long id;

	private String user;

	@RevisionTimestamp
	private Date date;

	public void setDate(LocalDateTime now) {
		now = this.date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
	}

	public LocalDateTime getDate() {
		return LocalDateTime.now();
	}

}
