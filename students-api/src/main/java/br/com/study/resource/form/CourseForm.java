package br.com.study.resource.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import br.com.study.model.Course;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CourseForm {

	@NotNull @NotEmpty
	private Long id;
	
	public Course converter() {
		return new Course(id);
	}
	
}
