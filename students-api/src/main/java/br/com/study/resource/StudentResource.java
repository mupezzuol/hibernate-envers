package br.com.study.resource;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.validation.Valid;

import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.query.AuditEntity;
import org.hibernate.envers.query.AuditQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.study.model.Student;
import br.com.study.resource.dto.StudentDto;
import br.com.study.resource.form.StudentForm;
import br.com.study.service.imp.StudentService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/students")
public class StudentResource {

	@Autowired
	private EntityManager entityManager;

	@Autowired
	private StudentService service;

	@PostMapping
	public ResponseEntity<StudentDto> registerStudent(@RequestBody @Valid StudentForm form,
			UriComponentsBuilder uriBuilder) {
		log.info("Register student");

		Student updateStudent = this.service.updateStudent(form);

		URI uri = uriBuilder.path("/students/{id}").buildAndExpand(updateStudent.getId()).toUri();

		log.info("Register student finalized ");

		return ResponseEntity.created(uri).body(new StudentDto(updateStudent));
	}

	// Informar o id do estudante e o número da revisão para retornar os dados
	@GetMapping("/audit")
	public ResponseEntity<StudentDto> getAudit(@RequestParam Long student, @RequestParam Long revision, Pageable pageable) {
		System.out.println("Reading audit...");

		AuditReader auditReader = AuditReaderFactory.get(entityManager);
		Student find = auditReader.find(Student.class, student, revision);

		System.out.println(find);

		return ResponseEntity.ok().body(new StudentDto(find));
	}

	// Informar o id do estudante para retornar todas as modificações
	@GetMapping("/revisions")
	public ResponseEntity<Page<StudentDto>> getRevisionsStudent(@RequestParam Long student, Pageable pageable) {
		System.out.println("Reading revisions...");
		
		AuditReader auditReader = AuditReaderFactory.get(entityManager);
		AuditQuery query = auditReader.createQuery().forRevisionsOfEntity(Student.class, false, true);

		// Retorna todas as modificações de um estudante
		List<Object[]> resultListStudentRevisions = auditReader.createQuery().forRevisionsOfEntity(Student.class, false, true)
				.add(AuditEntity.id().eq(student)).getResultList();
		
		List<StudentDto> listStudentRevisionsDto = new ArrayList<>();
		
		resultListStudentRevisions.forEach(objects -> {
			Student studentRev = (Student) objects[0];
			listStudentRevisionsDto.add(new StudentDto(studentRev));
		});
		
		// Convert List for Page
		Page<StudentDto> pagesStudenRevisionstDto = new PageImpl<StudentDto>(listStudentRevisionsDto, pageable, listStudentRevisionsDto.size());
		
		return ResponseEntity.ok().body(pagesStudenRevisionstDto);
	}
	
	@GetMapping("/revisions/all")
	public ResponseEntity<Page<StudentDto>> getRevisions(Pageable pageable) {
		AuditReader auditReader = AuditReaderFactory.get(entityManager);
		AuditQuery query = auditReader.createQuery().forRevisionsOfEntity(Student.class, false, true);
		
		// Retorna todas as modificações
		List<Object[]> resultRevisionsAll = query.getResultList();
		
		List<StudentDto> listRevisionsAllDto = new ArrayList<>();
		
		resultRevisionsAll.forEach(objects -> {
			Student studentRev = (Student) objects[0];
			listRevisionsAllDto.add(new StudentDto(studentRev));
		});
		
		// Convert List for Page
		Page<StudentDto> pagesRevisionsDto = new PageImpl<StudentDto>(listRevisionsAllDto, pageable, listRevisionsAllDto.size());
		
		return ResponseEntity.ok().body(pagesRevisionsDto);
	}

}
