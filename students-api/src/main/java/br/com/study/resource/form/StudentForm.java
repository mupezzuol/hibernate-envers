package br.com.study.resource.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import br.com.study.model.Student;
import br.com.study.repository.ICourseRepository;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class StudentForm {

	private Long id;

	@NotNull
	@NotEmpty
	@Length(min = 3)
	private String name;

	@NotNull
	private Integer age;

//	@NotNull
//	private Address address;

//	private CourseForm course;

	public Student converter(ICourseRepository repository) {
		// Optional<Course> courseFound = repository.findById(course.getId());

		return new Student(name, age);
	}

}
