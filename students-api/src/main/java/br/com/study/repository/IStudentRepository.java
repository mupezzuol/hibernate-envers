package br.com.study.repository;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import br.com.study.model.Student;

@Transactional
public interface IStudentRepository extends CrudRepository<Student, Long> {

}
